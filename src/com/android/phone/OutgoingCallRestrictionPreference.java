package com.android.phone;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.os.SystemProperties;
import android.preference.ListPreference;
import android.util.AttributeSet;
import android.provider.Settings;

import com.android.phone.R;

public final class OutgoingCallRestrictionPreference extends ListPreference {

  private static final int NO_RESTRICTION = 0;
  private static final int RESTRICT_ALL_OUTGOING_CALL = 1;
  private static final int RESTRICT_CONTACTS_LIST = 2;

  /**
   * Property to set outgoing call restriction state.
   * Type: int
   * 0 - RESTRICT_NONE.
   * 1 - RESTRICT_ALL_OUTGOING_CALL.
   * 2 - RESTRICT_CONTACTS_LIST.
   */
  static final String PROPERTY_OUTGOING_CALL_RESTRICTIONS = "vendor.sys.telephony.mo.restrict";

  private Context mContext;

  public OutgoingCallRestrictionPreference(Context context) {
    super(context);
    prepare();
  }

  public OutgoingCallRestrictionPreference(Context context, AttributeSet attrs) {
    super(context, attrs);
    prepare();
  }

  private void prepare() {
    mContext = getContext();
    setEntries(new String[]{
      mContext.getString(R.string.outgoing_call_restrictions_no_restricted),
      mContext.getString(R.string.outgoing_call_restrictions_all_outgoing_restricted),
      mContext.getString(R.string.outgoing_call_restrictions_contacts_list_restricted),
    });
    setEntryValues(new String[]{
      String.valueOf(NO_RESTRICTION),
      String.valueOf(RESTRICT_ALL_OUTGOING_CALL),
      String.valueOf(RESTRICT_CONTACTS_LIST),
    });
    setValue(String.valueOf(getCallRestrictionState()));
  }

  @Override
  protected boolean shouldPersist() {
    return false;   // This preference takes care of its own storage
  }

  @Override
  public CharSequence getSummary() {
    switch (getCallRestrictionState()) {
      case NO_RESTRICTION:
        return mContext.getString(R.string.outgoing_call_restrictions_no_restricted);
      case RESTRICT_ALL_OUTGOING_CALL:
        return mContext.getString(R.string.outgoing_call_restrictions_all_outgoing_restricted);
      case RESTRICT_CONTACTS_LIST:
        return mContext.getString(R.string.outgoing_call_restrictions_contacts_list_restricted);
    }
    return null;
  }

  @Override
  protected boolean persistString(String value) {
    int newValue = Integer.parseInt(value);
    if (newValue != getCallRestrictionState()) {
      setCallRestrictionState(newValue);
      notifyChanged();
    }
    return true;
  }

  @Override
  protected void onPrepareDialogBuilder(Builder builder) {
    super.onPrepareDialogBuilder(builder);
    builder.setNegativeButton(null, null);
  }

  private int getCallRestrictionState() {
    String outgoingCallRestriction = SystemProperties.get(PROPERTY_OUTGOING_CALL_RESTRICTIONS, Integer.toString(NO_RESTRICTION));
    return  Integer.parseInt(outgoingCallRestriction);
  }

  private void setCallRestrictionState(int value) {
    SystemProperties.set(PROPERTY_OUTGOING_CALL_RESTRICTIONS, Integer.toString(value));
  }
}
