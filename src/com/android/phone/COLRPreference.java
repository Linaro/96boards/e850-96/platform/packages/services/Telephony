/*
 * S.LSI Telephony Features
 * SS: COLP/COLR
 */

package com.android.phone;

import static com.android.phone.TimeConsumingPreferenceActivity.RESPONSE_ERROR;
import com.android.internal.telephony.*;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Phone;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;

public class COLRPreference extends Preference {
    private static final String LOG_TAG = "COLRPreference";
    private final boolean DBG = (PhoneGlobals.DBG_LEVEL >= 2);

    private final MyHandler mHandler = new MyHandler();
    private Phone mPhone;
    private TimeConsumingPreferenceListener mTcpListener;

    public COLRPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        mPhone = PhoneGlobals.getPhone();
    }

    public COLRPreference(Context context) {
        this(context, null);
    }

    void init(TimeConsumingPreferenceListener listener, boolean skipReading, Phone phone) {
        mTcpListener = listener;
        mPhone = phone;
        if (!skipReading) {
            mPhone.queryCOLR(mHandler.obtainMessage(MyHandler.MESSAGE_GET_COLR));
            if (mTcpListener != null) {
                mTcpListener.onStarted(this, true);
            }
        }
    }

    void handleGetCOLRResult(int tmpColr) {
        boolean enabled = true;

        int value = CommandsInterface.COLR_UNKNOWN;
        switch (tmpColr) {
            case 0: // Not Provisioned
                value = CommandsInterface.COLR_NOT_PROVISIONED;
                break;
            case 1: // Provisioned
                value = CommandsInterface.COLR_PROVISIONED;
                break;
            case 2: // Unknown (network error, etc)
            default:
                value = CommandsInterface.COLR_UNKNOWN;
                break;
        }
        setEnabled(enabled);

        // set the string summary to reflect the value
        int summary = R.string.sum_colr_unknown;
        switch (value) {
            case CommandsInterface.COLR_NOT_PROVISIONED:
                summary = R.string.sum_colr_not_provisioned;
                break;
            case CommandsInterface.COLR_PROVISIONED:
                summary = R.string.sum_colr_provisioned;
                break;
            case CommandsInterface.COLR_UNKNOWN:
                summary = R.string.sum_colr_unknown;
                break;
        }
        setSummary(summary);
    }

    private class MyHandler extends Handler {
        static final int MESSAGE_GET_COLR = 0;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_GET_COLR:
                    handleGetCOLRResponse(msg);
                    break;
            }
        }

        private void handleGetCOLRResponse(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;
            mTcpListener.onFinished(COLRPreference.this, true);

            if (ar.exception != null) {
                if (DBG) Log.d(LOG_TAG, "handleGetCOLRResponse: ar.exception=" + ar.exception);
                mTcpListener.onException(COLRPreference.this, (CommandException) ar.exception);
            } else if (ar.userObj instanceof Throwable) {
                mTcpListener.onError(COLRPreference.this, RESPONSE_ERROR);
            } else {
                int colrArray[] = (int[]) ar.result;
                if (colrArray == null || colrArray.length != 1) {
                    mTcpListener.onError(COLRPreference.this, RESPONSE_ERROR);
                } else {
                    if (DBG) {
                        Log.d(LOG_TAG, "handleGetCOLRResponse: COLR successfully queried,"
                                + " colrArray[0]=" + colrArray[0]);
                    }
                    handleGetCOLRResult(colrArray[0]);
                }
            }
        }
    }
}
