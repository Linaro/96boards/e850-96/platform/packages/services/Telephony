/*
 * S.LSI Telephony Features
 * SS: COLP/COLR
 */

package com.android.phone;

import static com.android.phone.TimeConsumingPreferenceActivity.RESPONSE_ERROR;
import com.android.internal.telephony.*;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Phone;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;

public class COLPPreference extends Preference {
    private static final String LOG_TAG = "COLPPreference";
    private final boolean DBG = (PhoneGlobals.DBG_LEVEL >= 2);

    private final MyHandler mHandler = new MyHandler();
    private Phone mPhone;
    private TimeConsumingPreferenceListener mTcpListener;

    public COLPPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPhone = PhoneGlobals.getPhone();
    }

    public COLPPreference(Context context) {
        this(context, null);
    }

    void init(TimeConsumingPreferenceListener listener, boolean skipReading, Phone phone) {
        mTcpListener = listener;
        mPhone = phone;
        if (!skipReading) {
            mPhone.queryCOLP(mHandler.obtainMessage(MyHandler.MESSAGE_GET_COLP));
            if (mTcpListener != null) {
                mTcpListener.onStarted(this, true);
            }
        }
    }

    void handleGetCOLPResult(int tmpColp) {
        boolean enabled = true;

        int value = CommandsInterface.COLP_UNKNOWN;
        switch (tmpColp) {
            case 0: // Not Provisioned
                value = CommandsInterface.COLP_NOT_PROVISIONED;
                break;
            case 1: // Provisioned
                value = CommandsInterface.COLP_PROVISIONED;
                break;
            case 2: // Unknown (network error, etc)
            default:
                value = CommandsInterface.COLP_UNKNOWN;
                break;
        }
        setEnabled(enabled);

        // set the string summary to reflect the value
        int summary = R.string.sum_colp_unknown;
        switch (value) {
            case CommandsInterface.COLP_NOT_PROVISIONED:
                summary = R.string.sum_colp_not_provisioned;
                break;
            case CommandsInterface.COLP_PROVISIONED:
                summary = R.string.sum_colp_provisioned;
                break;
            case CommandsInterface.COLP_UNKNOWN:
                summary = R.string.sum_colp_unknown;
                break;
        }
        setSummary(summary);
    }

    private class MyHandler extends Handler {
        static final int MESSAGE_GET_COLP = 0;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_GET_COLP:
                    handleGetCOLPResponse(msg);
                    break;
            }
        }

        private void handleGetCOLPResponse(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;
            mTcpListener.onFinished(COLPPreference.this, true);

            if (ar.exception != null) {
                if (DBG) Log.d(LOG_TAG, "handleGetCOLPResponse: ar.exception=" + ar.exception);
                mTcpListener.onException(COLPPreference.this, (CommandException) ar.exception);
            } else if (ar.userObj instanceof Throwable) {
                mTcpListener.onError(COLPPreference.this, RESPONSE_ERROR);
            } else {
                int colpArray[] = (int[]) ar.result;
                if (colpArray == null || colpArray.length != 1) {
                    mTcpListener.onError(COLPPreference.this, RESPONSE_ERROR);
                } else {
                    if (DBG) {
                        Log.d(LOG_TAG, "handleGetCOLPResponse: COLP successfully queried,"
                                + " colpArray[0]=" + colpArray[0]);
                    }
                    handleGetCOLPResult(colpArray[0]);
                }
            }
        }
    }
}
