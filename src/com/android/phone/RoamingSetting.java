package com.android.phone;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.UserHandle;
import android.os.UserManager;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.util.Log;

import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import com.android.settingslib.RestrictedLockUtilsInternal;

import com.android.phone.roaming.RoamingSettingProvider;

/**
 * List of Roaming guard settings screens.
 */
public class RoamingSetting extends PreferenceActivity {
    private static final String LOG_TAG = "RoamingSetting";

    public static final String ROAMING_SETTING_KEY = "roaming_setting_key";
    public static final String DOMESTIC_VOICE_SMS_KEY = "domestic_voice_sms_key";
    public static final String DOMESTIC_DATA_KEY = "domestic_data_key";
    public static final String INTERNATIONAL_VOICE_SMS_KEY = "international_voice_sms_key";
    public static final String INTERNATIONAL_DATA_KEY = "international_data_key";

    private RestrictedSwitchPreference mButtonRoam;

    private SwitchPreference mDomesticVoiceSms;
    private RestrictedSwitchPreference mDomesticData;
    private SwitchPreference mInternationalVoiceSms;
    private RestrictedSwitchPreference mInternationalData;

    private Phone mPhone;
    private SubscriptionManager mSubscriptionManager;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        addPreferencesFromResource(R.xml.roaming_setting);

        int subId = getIntent().getIntExtra("subId",-1);
        int phoneId = (subId == -1) ? -1 : SubscriptionManager.getPhoneId(subId);
        if (phoneId < 0) {
            mPhone = PhoneGlobals.getPhone();
        } else {
            mPhone = PhoneFactory.getPhone(phoneId);
        }

        mButtonRoam = (RestrictedSwitchPreference) findPreference(ROAMING_SETTING_KEY);
        mDomesticVoiceSms = (SwitchPreference) findPreference(DOMESTIC_VOICE_SMS_KEY);
        mDomesticData = (RestrictedSwitchPreference) findPreference(DOMESTIC_DATA_KEY);
        mInternationalVoiceSms = (SwitchPreference) findPreference(INTERNATIONAL_VOICE_SMS_KEY);
        mInternationalData = (RestrictedSwitchPreference) findPreference(INTERNATIONAL_DATA_KEY);

        mSubscriptionManager = SubscriptionManager.from(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        // set the default setting. If default setting is needed,
        // use setDefaultSetting instead of loadSettings.
        // setDefaultSetting();

        loadSettings();

        boolean hasActiveSubscriptions = hasActiveSubscriptions();
        if (mButtonRoam.isChecked() && hasActiveSubscriptions) {
            mButtonRoam.setTitle(R.string.roaming_on);
            mButtonRoam.setEnabled(true);
            setRoamingOptions(true);
        } else {
            mButtonRoam.setTitle(R.string.roaming_off);
            mButtonRoam.setEnabled(false);
            setRoamingOptions(false);
        }

        updateRoamingSettings();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.roaming_guard_alert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (itemId == R.id.menu_roaming_alert) {
            // show RoamingAlertDialog
            RoamingAlertDialog dialog = new RoamingAlertDialog();
            dialog.show(getFragmentManager(), "RoamingAlertDialog");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference == mInternationalData) {
            if (mInternationalData.isChecked()) {
                mPhone.setDataRoamingEnabled(true);
                final PhoneGlobals app = PhoneGlobals.getInstance();
                app.notificationMgr.hideDataDisconnectedRoaming();
            } else {
                mPhone.setDataRoamingEnabled(false);
            }
        } else if (preference == mButtonRoam) {
            if (mButtonRoam.isChecked()) {
                mButtonRoam.setTitle(R.string.roaming_on);
                setRoamingOptions(true);
            } else {
                mButtonRoam.setTitle(R.string.roaming_off);
                setRoamingOptions(false);
            }
        }
        updateRoamingSettings();
        return true;
    }

    private void setDefaultSetting() {
        mButtonRoam.setChecked(true);
        mButtonRoam.setTitle(R.string.roaming_on);
        mDomesticVoiceSms.setChecked(true);
        mInternationalVoiceSms.setChecked(true);
        mDomesticData.setChecked(false);
        mInternationalData.setChecked(true);
    }

    private void updateRoamingSettings() {
        Log.d(LOG_TAG, "updateRoamingSettings"
                       + " ROAMING_SETTING_KEY- isChecked: " + mButtonRoam.isChecked()
                       + " DOMESTIC_VOICE_SMS_KEY- isChecked: " + mDomesticVoiceSms.isChecked()
                       + " DOMESTIC_DATA_KEY- isChecked: " + mDomesticData.isChecked()
                       + " INTERNATIONAL_VOICE_SMS_KEY- isChecked: " + mInternationalVoiceSms.isChecked()
                       + " INTERNATIONAL_DATA_KEY: " + mInternationalData.isChecked());

        ContentValues values = new ContentValues();
        values.put(ROAMING_SETTING_KEY, getInt(mButtonRoam.isChecked()));
        values.put(DOMESTIC_VOICE_SMS_KEY, getInt(mDomesticVoiceSms.isChecked()));
        values.put(DOMESTIC_DATA_KEY, getInt(mDomesticData.isChecked()));
        values.put(INTERNATIONAL_VOICE_SMS_KEY, getInt(mInternationalVoiceSms.isChecked()));
        values.put(INTERNATIONAL_DATA_KEY, getInt(mInternationalData.isChecked()));
        getApplicationContext().getContentResolver().update(
            RoamingSettingProvider.CONTENT_URI, values, "_id=1", null);
    }

    private boolean hasActiveSubscriptions() {
        return mSubscriptionManager.getActiveSubscriptionInfoCount() > 0;
    }

    private void setRoamingOptions(boolean enabled) {
        mDomesticVoiceSms.setEnabled(enabled);
        mDomesticData.setEnabled(enabled);
        mInternationalVoiceSms.setEnabled(enabled);
        mInternationalData.setEnabled(enabled);
        mInternationalData.setDisabledByAdmin(false);
        if (mInternationalData.isEnabled()) {
            if (RestrictedLockUtilsInternal.hasBaseUserRestriction(getApplicationContext(),
                        UserManager.DISALLOW_DATA_ROAMING, UserHandle.myUserId())) {
            } else {
                mInternationalData.checkRestrictionAndSetDisabled(UserManager.DISALLOW_DATA_ROAMING);
            }
        }

        // InternationalData
        mPhone.setDataRoamingEnabled(mInternationalData.isChecked());
        if (mInternationalData.isChecked()) {
            final PhoneGlobals app = PhoneGlobals.getInstance();
            app.notificationMgr.hideDataDisconnectedRoaming();
        }
    }

    private void loadSettings() {
        try {
            Cursor c = getApplicationContext().getContentResolver().query(
                RoamingSettingProvider.CONTENT_URI, new String[]{}, null, null, null);
            if (c != null && c.moveToFirst()) {
                boolean roamingSetting = getBoolean(c.getInt(1));
                boolean domesticVoiceSms = getBoolean(c.getInt(2));
                boolean domesticData = getBoolean(c.getInt(3));
                boolean internationalVoiceSms = getBoolean(c.getInt(4));
                boolean internationalData = getBoolean(c.getInt(5));

                Log.d(LOG_TAG, "loadSettings"
                       + " roamingSetting: " + roamingSetting
                       + " domesticVoiceSms: " + domesticVoiceSms
                       + " domesticData: " + domesticData
                       + " internationalVoiceSms: " + internationalVoiceSms
                       + " internationalData: " + internationalData);

                mButtonRoam.setChecked(roamingSetting);
                mDomesticVoiceSms.setChecked(domesticVoiceSms);
                mInternationalVoiceSms.setChecked(internationalVoiceSms);
                mDomesticData.setChecked(domesticData);
                mInternationalData.setChecked(internationalData);
            } else {
                Log.d(LOG_TAG, "loadSettings can't find item");
            }
        } catch (IllegalArgumentException ex1) {
            Log.d(LOG_TAG, "IllegalArgumentException: " + ex1);
        } catch (Exception ex2) {
            Log.d(LOG_TAG, "Exception: " + ex2);
        }
    }

    private int getInt(boolean value) {
        return value ? 1 : 0;
    }

    private boolean getBoolean(int value) {
        return value == 1;
    }
}
