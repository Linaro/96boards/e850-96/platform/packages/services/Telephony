package com.android.phone;

import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Phone;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.AttributeSet;
import android.util.Log;

public class CdmaCallWaitingPreference extends Preference {
    private static final String LOG_TAG = "CdmaCallWaitingPreference";
    private static final boolean DBG = (PhoneGlobals.DBG_LEVEL >= 2);

    private int mButtonClicked;
    private Context mContext;
    private Phone mPhone;
    private SubscriptionInfoHelper mSubscriptionInfoHelper;
    private MyHandler mHandler = new MyHandler();

    public CdmaCallWaitingPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public CdmaCallWaitingPreference(Context context, AttributeSet attrs) {
        this(context, attrs, com.android.internal.R.attr.preferenceStyle);
    }

    public CdmaCallWaitingPreference(Context context) {
        this(context, null);
    }

    public void init(Phone phone) {
        mPhone = phone;
        Log.d(LOG_TAG, "phone id= " + mPhone.getPhoneId());
        mPhone.getCallWaiting(mHandler.obtainMessage(MyHandler.MESSAGE_GET_CALL_WAITING,
                    MyHandler.MESSAGE_GET_CALL_WAITING, MyHandler.MESSAGE_GET_CALL_WAITING));
    }

    @Override
    public void onClick() {
        super.onClick();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getText(R.string.cdma_call_waiting));
        builder.setMessage(mContext.getText(R.string.enable_cdma_call_waiting_setting));
        builder.setPositiveButton(R.string.enable_cdma_cw, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mPhone.setCallWaiting(true, mHandler.obtainMessage(MyHandler.MESSAGE_SET_CALL_WAITING));
            }
        });
        builder.setNegativeButton(R.string.disable_cdma_cw, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mPhone.setCallWaiting(false, mHandler.obtainMessage(MyHandler.MESSAGE_SET_CALL_WAITING));
            }
        });
        builder.create().show();
    }

    private class MyHandler extends Handler {
        static final int MESSAGE_GET_CALL_WAITING = 0;
        static final int MESSAGE_SET_CALL_WAITING = 1;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_GET_CALL_WAITING:
                    handleGetCallWaitingResponse(msg);
                    break;
                case MESSAGE_SET_CALL_WAITING:
                    handleSetCallWaitingResponse(msg);
                    break;
            }
        }

        private void handleGetCallWaitingResponse(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;

            if (ar.exception instanceof CommandException) {
                if (DBG) {
                    Log.d(LOG_TAG, "handleGetCallWaitingResponse: CommandException=" +
                            ar.exception);
                }
            } else if (ar.userObj instanceof Throwable || ar.exception != null) {
                if (DBG) {
                    Log.d(LOG_TAG, "handleGetCallWaitingResponse: Exception" + ar.exception);
                }
            } else {
                if (DBG) {
                    Log.d(LOG_TAG, "handleGetCallWaitingResponse: CW state successfully queried.");
                }
                int[] cwArray = (int[])ar.result;
                if (cwArray == null) {
                    return;
                }

                if (cwArray[0] == 2) {
                    setSummary("");
                }

                if(cwArray[0] == 1) {
                    setSummary(mContext.getString(R.string.cdma_call_waiting_in_ims_on));
                } else if(cwArray[0] == 0) {
                    setSummary(mContext.getString(R.string.cdma_call_waiting_in_ims_off));
                }

                try {
                } catch (ArrayIndexOutOfBoundsException e) {
                    Log.e(LOG_TAG, "handleGetCallWaitingResponse: improper result: err ="
                            + e.getMessage());
                }
            }
        }

        private void handleSetCallWaitingResponse(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;

            if (ar.exception != null) {
                if (DBG) {
                    Log.d(LOG_TAG, "handleSetCallWaitingResponse: ar.exception=" + ar.exception);
                }
            }

            if (ar.result != null) {
                int arr = (int)ar.result;
                if (arr == 2) {
                    Log.d(LOG_TAG, "handleSetCallWaitingResponse: no need to re get in CDMA");
                    return;
                }
            }

            if (DBG) Log.d(LOG_TAG, "handleSetCallWaitingResponse: re get");
            mPhone.getCallWaiting(obtainMessage(MESSAGE_GET_CALL_WAITING,
                        MESSAGE_SET_CALL_WAITING, MESSAGE_SET_CALL_WAITING, ar.exception));
        }
    }
}
