package com.android.phone;

import android.content.Intent;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import com.android.phone.R;
import com.android.phone.SubscriptionInfoHelper;

public class RestrictionSettings extends PreferenceActivity {

    private SubscriptionInfoHelper mSubscriptionInfoHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptionInfoHelper = new SubscriptionInfoHelper(this, getIntent());
        addPreferencesFromResource(R.xml.restriction_settings);
    }
}
