package com.android.phone;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.phone.roaming.RoamingSettingProvider;

public class RoamingAlertDialog extends DialogFragment implements CompoundButton.OnCheckedChangeListener
{
    private static final String LOG_TAG = "RoamingAlertDialog";

    public static final String DOMESTIC_VOICE_ALERT_KEY      = "domestic_voice_alert_key";
    public static final String DOMESTIC_DATA_ALERT_KEY       = "domestic_data_alert_key";
    public static final String INTERNATIONAL_VOICE_ALERT_KEY = "international_voice_alert_key";
    public static final String INTERNATIONAL_DATA_ALERT_KEY  = "international_data_alert_key";
    public static final String INTERNATIONAL_SMS_ALERT_KEY   = "international_sms_alert_key";

    private CheckBox mCkbDomesticVoice = null;
    private CheckBox mCkbDomesticData = null;
    private CheckBox mCkbInternationalVoice = null;
    private CheckBox mCkbInternationalData = null;
    private CheckBox mCkbInternationalSms = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View dialogView = inflater.inflate(R.xml.roaming_alert_dialog, container, false);
        getDialog().setTitle(R.string.roaming_alert);
        return dialogView;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.xml.roaming_alert_dialog, null);

        mCkbDomesticVoice = (CheckBox) dialogView.findViewById(R.id.ckBtn_domestic_voice_alert);
        mCkbDomesticData = (CheckBox) dialogView.findViewById(R.id.ckBtn_domestic_data_alert);
        mCkbInternationalVoice = (CheckBox) dialogView.findViewById(R.id.ckBtn_international_voice_alert);
        mCkbInternationalData = (CheckBox) dialogView.findViewById(R.id.ckBtn_international_data_alert);
        mCkbInternationalSms = (CheckBox) dialogView.findViewById(R.id.ckBtn_international_sms_alert);
        mCkbDomesticVoice.setOnCheckedChangeListener(this);
        mCkbDomesticData.setOnCheckedChangeListener(this);
        mCkbInternationalVoice.setOnCheckedChangeListener(this);
        mCkbInternationalData.setOnCheckedChangeListener(this);
        mCkbInternationalSms.setOnCheckedChangeListener(this);

        builder.setView(dialogView);
        builder.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                updateRoamingGuard();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        init();
        return builder.create();
    }

    private void init() {
        try {
            Cursor c = getActivity().getApplicationContext().getContentResolver().query(
                    RoamingSettingProvider.CONTENT_URI, new String[]{}, null, null, null);
            if (c != null && c.moveToFirst()) {
                boolean ckbDomesticVoice = getBoolean(c.getInt(5));
                boolean ckbDomesticData = getBoolean(c.getInt(6));
                boolean ckbInternationalVoice = getBoolean(c.getInt(7));
                boolean ckbInternationalData = getBoolean(c.getInt(8));
                boolean ckbInternationalSms = getBoolean(c.getInt(9));

                Log.d(LOG_TAG, "init"
                        + " ckbDomesticVoice: " + ckbDomesticVoice
                        + " ckbDomesticData: " + ckbDomesticData
                        + " ckbInternationalVoice: " + ckbInternationalVoice
                        + " ckbInternationalData: " + ckbInternationalData
                        + " ckbInternationalSms: " + ckbInternationalSms);

                mCkbDomesticVoice.setChecked(ckbDomesticVoice);
                mCkbDomesticData.setChecked(ckbDomesticData);
                mCkbInternationalVoice.setChecked(ckbInternationalVoice);
                mCkbInternationalData.setChecked(ckbInternationalData);
                mCkbInternationalSms.setChecked(ckbInternationalSms);
            } else {
                Log.d(LOG_TAG, "init can't find item");
            }
        } catch (IllegalArgumentException ex1) {
            Log.d(LOG_TAG, "IllegalArgumentException: " + ex1);
        } catch (Exception ex2) {
            Log.d(LOG_TAG, "Exception: " + ex2);
        }
    }

    private void updateRoamingGuard() {
        Log.d(LOG_TAG, "updateRoamingGuard: domestic voice=" + mCkbDomesticVoice.isChecked()
                        + " domestic data=" + mCkbDomesticData.isChecked()
                        + " international voice=" + mCkbInternationalVoice.isChecked()
                        + " international data=" + mCkbInternationalData.isChecked()
                        + " international sms=" + mCkbInternationalSms.isChecked());

        ContentValues values = new ContentValues();
        values.put(DOMESTIC_VOICE_ALERT_KEY, getInt(mCkbDomesticVoice.isChecked()));
        values.put(DOMESTIC_DATA_ALERT_KEY, getInt(mCkbDomesticData.isChecked()));
        values.put(INTERNATIONAL_VOICE_ALERT_KEY, getInt(mCkbInternationalVoice.isChecked()));
        values.put(INTERNATIONAL_DATA_ALERT_KEY, getInt(mCkbInternationalData.isChecked()));
        values.put(INTERNATIONAL_SMS_ALERT_KEY, getInt(mCkbInternationalSms.isChecked()));
        getActivity().getApplicationContext().getContentResolver().update(
                RoamingSettingProvider.CONTENT_URI, values, "_id=1", null);
    }

    private int getInt(boolean value) {
        return value ? 1 : 0;
    }

    private boolean getBoolean(int value) {
        return value == 1;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    }
}
