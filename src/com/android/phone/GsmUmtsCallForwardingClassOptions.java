/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.phone;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.view.MenuItem;

import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;

import android.util.Log;

public class GsmUmtsCallForwardingClassOptions extends PreferenceActivity {
    private static final String LOG_TAG = "GsmUmtsCallForwardingClassOptions";
    private final boolean DBG = (PhoneGlobals.DBG_LEVEL >= 2);

    private static final String CALL_FORWARDING_VOICE_KEY = "call_forwarding_voice_key";
    private static final String CALL_FORWARDING_VIDEO_KEY = "call_forwarding_video_key";

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        addPreferencesFromResource(R.xml.gsm_umts_call_forwarding_class_options);

        SubscriptionInfoHelper subInfoHelper = new SubscriptionInfoHelper(this, getIntent());
        subInfoHelper.setActionBarTitle(
                getActionBar(), getResources(), R.string.labelGsmMore_with_label);

        init(getPreferenceScreen(), subInfoHelper);

        if (subInfoHelper.getPhone().getPhoneType() != PhoneConstants.PHONE_TYPE_GSM) {
            //disable the entire screen
            getPreferenceScreen().setEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init(PreferenceScreen prefScreen, SubscriptionInfoHelper subInfoHelper) {
        Preference callForwardingVoicePref = prefScreen.findPreference(CALL_FORWARDING_VOICE_KEY);
        Intent intent = subInfoHelper.getIntent(GsmUmtsCallForwardOptions.class);
        intent.putExtra(SubscriptionInfoHelper.SUB_SERVICE_CLASS_EXTRA,
                        CommandsInterface.SERVICE_CLASS_VOICE);
        callForwardingVoicePref.setIntent(intent);

        Preference callForwardingVideoPref = prefScreen.findPreference(CALL_FORWARDING_VIDEO_KEY);
        intent = subInfoHelper.getIntent(GsmUmtsCallForwardOptions.class);
        intent.putExtra(SubscriptionInfoHelper.SUB_SERVICE_CLASS_EXTRA,
                        CommandsInterface.SERVICE_CLASS_DATA_SYNC); // it should be changed
        callForwardingVideoPref.setIntent(intent);
    }
}
