package com.android.phone.roaming;

import android.content.Context;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class RoamingSettingOpenHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = "RoamingSettingOpenHelper";

    public static final int DATABASE_VERSION = 10;
    public static final String DATABASE_NAME = "roaming.db";
    public static final String TABLE_NAME = "roaming";

    public static final String ID_KEY = "_id";
    public static final String ROAMING_SETTING_KEY = "roaming_setting_key";
    public static final String DOMESTIC_VOICE_SMS_KEY = "domestic_voice_sms_key";
    public static final String DOMESTIC_DATA_KEY = "domestic_data_key";
    public static final String INTERNATIONAL_VOICE_SMS_KEY = "international_voice_sms_key";
    public static final String INTERNATIONAL_DATA_KEY = "international_data_key";

    public static final String DOMESTIC_VOICE_ALERT_KEY      = "domestic_voice_alert_key";
    public static final String DOMESTIC_DATA_ALERT_KEY       = "domestic_data_alert_key";
    public static final String INTERNATIONAL_VOICE_ALERT_KEY = "international_voice_alert_key";
    public static final String INTERNATIONAL_DATA_ALERT_KEY  = "international_data_alert_key";
    public static final String INTERNATIONAL_SMS_ALERT_KEY   = "international_sms_alert_key";

    public static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS " +
                               TABLE_NAME + " (" +
                               ID_KEY + " INTEGER PRIMARY KEY," +
                               ROAMING_SETTING_KEY + " INTEGER DEFAULT 1, " +
                               DOMESTIC_VOICE_SMS_KEY + " INTEGER DEFAULT 1, " +
                               DOMESTIC_DATA_KEY + " INTEGER DEFAULT 0, " +
                               INTERNATIONAL_VOICE_SMS_KEY + " INTEGER DEFAULT 1, " +
                               INTERNATIONAL_DATA_KEY + " INTEGER DEFAULT 1, " +
                               DOMESTIC_VOICE_ALERT_KEY + " INTEGER DEFAULT 0, " +
                               DOMESTIC_DATA_ALERT_KEY + " INTEGER DEFAULT 0, " +
                               INTERNATIONAL_VOICE_ALERT_KEY + " INTEGER DEFAULT 1, " +
                               INTERNATIONAL_DATA_ALERT_KEY + " INTEGER DEFAULT 0, " +
                               INTERNATIONAL_SMS_ALERT_KEY + " INTEGER DEFAULT 1);";

    public RoamingSettingOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "onCreate");
        setupTables(db);
        init(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(LOG_TAG, "onUpgrade");
        setupTables(db);
        init(db);
    }

    private void setupTables(SQLiteDatabase db) {
        Log.d(LOG_TAG, "setupTables");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL(DATABASE_CREATE);
    }

    private void init(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(ROAMING_SETTING_KEY, 1);
        values.put(DOMESTIC_VOICE_SMS_KEY, 1);
        values.put(DOMESTIC_DATA_KEY, 0);
        values.put(INTERNATIONAL_VOICE_SMS_KEY, 1);
        values.put(INTERNATIONAL_DATA_KEY, 1);
        db.insert(TABLE_NAME, null, values);
    }
}
