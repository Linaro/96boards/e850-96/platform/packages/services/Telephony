package com.android.phone.roaming;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class RoamingSettingProvider extends ContentProvider {

    private static final String LOG_TAG = "RoamingSettingProvider";

    private RoamingSettingOpenHelper mRoamingSettingOpenHelper;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    public static final String AUTHORITY = "com.android.phone.roaming.roamingprovider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + RoamingSettingOpenHelper.TABLE_NAME);

    private static final int ROAMING_TABLE = 1;
    private static final int ROAMING_TABLE_ID = 2;

    @Override
    public boolean onCreate() {
        mRoamingSettingOpenHelper = new RoamingSettingOpenHelper(getContext());
        if (mRoamingSettingOpenHelper == null) {
            Log.d(LOG_TAG, "onCreate mRoamingSettingOpenHelper is null");
            return false;
        }
        sUriMatcher.addURI(AUTHORITY, RoamingSettingOpenHelper.TABLE_NAME, ROAMING_TABLE);
        sUriMatcher.addURI(AUTHORITY, RoamingSettingOpenHelper.TABLE_NAME + "/#", ROAMING_TABLE_ID);
        return true;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.d(LOG_TAG, "insert");
        SQLiteDatabase db = mRoamingSettingOpenHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROAMING_TABLE:
                break;
            case ROAMING_TABLE_ID:
                break;
            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        long rows = db.insert(RoamingSettingOpenHelper.TABLE_NAME, null, values);
        if (rows < 0) {
            return null;
        }
        notifyChange(uri);
        return ContentUris.withAppendedId(uri, rows);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d(LOG_TAG, "update");
        SQLiteDatabase db = mRoamingSettingOpenHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROAMING_TABLE:
                break;
            case ROAMING_TABLE_ID:
                selection = getSelectionWithId(selection, ContentUris.parseId(uri));
                break;
            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        int rows = db.update(RoamingSettingOpenHelper.TABLE_NAME, values, selection, selectionArgs);
        if (rows > 0) {
            notifyChange(uri);
        }
        return rows;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(LOG_TAG, "query");
        final SQLiteDatabase db = mRoamingSettingOpenHelper.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(RoamingSettingOpenHelper.TABLE_NAME);
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROAMING_TABLE:
                break;
            case ROAMING_TABLE_ID:
                qb.appendWhere(mRoamingSettingOpenHelper.ID_KEY + "=" + ContentUris.parseId(uri));
                break;
            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }
        final Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, null);
        if (c != null) {
            c.setNotificationUri(
                  getContext().getContentResolver(), CONTENT_URI);
        } else {
            Log.d(LOG_TAG, "CURSOR WAS NULL");
        }
        return c;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.d(LOG_TAG, "delete");
        SQLiteDatabase db = mRoamingSettingOpenHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROAMING_TABLE:
                break;
            case ROAMING_TABLE_ID:
                selection = getSelectionWithId(selection, ContentUris.parseId(uri));
                break;
            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        int rows = db.delete(RoamingSettingOpenHelper.TABLE_NAME, selection, selectionArgs);
        if (rows > 0) {
            notifyChange(uri);
        }
        return rows;
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROAMING_TABLE:
                return "vnd.android.cursor.dir/roaming";
            case ROAMING_TABLE_ID:
                return "vnd.android.cursor.item/roaming";
            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }
    }

    private String getSelectionWithId(String selection, long id) {
        if (TextUtils.isEmpty(selection)) {
            return RoamingSettingOpenHelper.ID_KEY + "=" + id;
        } else {
            return selection + "AND " + RoamingSettingOpenHelper.ID_KEY + "=" + id;
        }
    }

    private void notifyChange(Uri uri) {
        Log.d(LOG_TAG, "notifyChange");
        getContext().getContentResolver().notifyChange(uri, null);
    }
}
